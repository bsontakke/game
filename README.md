## To Clone Repository
	- git clone https://bsontakke@bitbucket.org/bsontakke/game.git
## Go To Project Directory
	- cd game
## Start Local Development Server
 	- php artisan serve
## Check Team A & Team B Result
## Call Throw Postman

	- curl --location --request POST 'http://127.0.0.1:8000/check-result' \
	--header 'Cookie: XSRF-TOKEN=eyJpdiI6IjR6Nm0vTXZLb3NZZDZOUFlKbzh1WkE9PSIsInZhbHVlIjoieWNUc3RKRW51dGx4cXJCMWZ0REgyeHRKd2NicmhOTmFGR1kra3ZoSm5STnhhMjNRWU1pSmRxdW0vL0ZScjBWRyIsIm1hYyI6ImZmODBiYWQ3NzE5OTljYmMyZDA4NWZlMDY3YzNmNjEzYmMxMTdlZTk2Y2EyN2NiMjNiOGZjNThlNWY1YTY0NmUifQ%3D%3D; laravel_session=eyJpdiI6IjNNN0RuZDNyTHV3WTVzMlR2YzVPZGc9PSIsInZhbHVlIjoiMjllS1J1UkI5M3NvT1ByVmpFSXRhdzZmRE9zc2FnVHdPZzFoTm9XRjFxZ21wRTEwZ25ZbVdNYTdNZzh6MnBUMSIsIm1hYyI6ImU4NTk1YzFjYWU1ODQ3NjMyY2M4MmU3OTkwNTBhZDM3ZTg3MjhmZTc5NTQ0ZGZhOTljMmU4MTE2Mzc2Y2YwOTgifQ%3D%3D' \
	--form 'teamA=35,70,20,50,40' \
	--form 'teamB=35,70,20,50,40'

## Call throw artisan command
	- php artisan command:game /check-result 35,100,20,50,40 35,10,30,20,90

## Run Test Cases
	- vendor/bin/phpunit