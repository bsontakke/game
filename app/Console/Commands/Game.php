<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class Game extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:game {url} {teamA} {teamB}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'php artisan command:game /check-result 35,100,20,50,40 35,10,30,20,90';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $url = $this->argument('url');
        $teamA = $this->argument('teamA');
        $teamB = $this->argument('teamB');

        $body = [
            'teamA' => $teamA,
            'teamB' => $teamB
        ];

        $body = [
            'teamA' => $teamA,
            'teamB' => $teamB
        ];

        $this->checkResult($url, $body);

        if ($this->confirm('Do you wish to continue? [yes|no]')) {

            $this->info('Please Enter Team with Comma Seperated(35,45,20,50,40 & 35,10,30,20,90)');
            $teamA = $this->ask('Enter Team A :- ');
            $teamB = $this->ask('Enter Team B :- ');

            $body = [
                'teamA' => $teamA,
                'teamB' => $teamB
            ];
            $this->checkResult($url, $body);
        }
    }

    public function checkResult($url, $body) {
        $this->info('Please wait for checking result...');
        $request = Request::create($url, 'POST');
        $request->request->add($body);
        $this->info(app()['Illuminate\Contracts\Http\Kernel']->handle($request));
    }
}
