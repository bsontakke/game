<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

/**
 * Game Controller use for handle team and check result
 *
 */
class GameController extends Controller
{
	/**
	* Compare Team A with B & evalute the result
	* @param string $teamB
	* @param string $teamA
	* @return array $response[]
	*/
    public function checkGameResult(Request $request) {
    	try {

    		$response = [
    			'status' => 0,
    			'data' => $request->all(),
    			'message' => 'Something went wrong.'
    		];

            // explode string to array
            $teamA = explode(",", $request->get('teamA'));
            $teamB = explode(",", $request->get('teamB'));

            // check for tie match
            if(!array_diff($teamA,$teamB)) {
				$response['message'] = "Tie";
				// Return the response
				return response()->json($response);
            }

			// sort team array for minimum iteration and team comparison
			rsort($teamA);
			rsort($teamB);
			$result = 1;
			// iteration team for comparison
			foreach ($teamA as $key => $value) {
				// comparison Team A with Team B IT get fasle then break the loop
				if($value < $teamB[$key]) {
					$result = 0;
					break;
				}
			}

			$response['status'] = $result;

			if($result) {
				$response['message'] = "Win";
			} else {
				$response['message'] = "Lose";
			}

			// Return the response
			return response()->json($response);

    	} catch (\Exception $e) {
    		// Hangle the Exception
    		$response['status'] = 0;
    		$response['message'] = $e->getMessage();
    		return response()->json($response, 500);
    	}
    }
}
