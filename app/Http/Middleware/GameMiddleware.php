<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Validator;
class GameMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {

            $response = [
                'status' => 0,
                'data' => $request->all(),
                'message' => 'Something went wrong.'
            ];

            // Validate the request
            $inputs = $request->all();
            $rules = [
                'teamA' => 'required',
                'teamB' => 'required',
            ];

            $messages = [
                'teamA.required' => 'Team A is required',
                'teamB.required' => 'Team B is required',
            ];

            $validator = Validator::make($inputs, $rules, $messages);

            // If fails then return error message
            if ($validator->fails()) {
                $response['message'] = $validator->errors()->first();
                return response()->json($response, 400);
            }

            $teamA = $request->get('teamA');
            $teamB = $request->get('teamB');
            // explode string to array
            $teamA = explode(",", $teamA);
            $teamB = explode(",", $teamB);

            if(count($teamA) != count($teamB)) {
                $response['message'] = 'Team Size is not same';
                return response()->json($response, 400);
            }

            return $next($request);

        } catch (\Exception $e) {
            // Hangle the Exception
            $response['status'] = 0;
            $response['message'] = $e->getMessage();
            return response()->json($response, 500);
        }
    }
}
