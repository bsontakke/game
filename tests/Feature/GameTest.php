<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use WithoutMiddleware;
use Illuminate\Support\Facades\Artisan;

class GameTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * Validate Input Param
     *
     * @return void
     */
    public function testValidation()
    {
        $body = [
            'teamA' => '35,100,20,50,40',
        ];
        $this->json('POST', '/check-result', $body, ['Accept' => 'application/json'])
        ->assertStatus(400)
        ->assertJson(['message' => "Team B is required"]);
    }

    /**
     * Test Win Response
     *
     * @return void
     */
    public function testWin()
    {
        $body = [
            'teamA' => '35,100,20,50,40',
            'teamB' => '35,10,30,20,90',
        ];

        $this->json('POST', '/check-result', $body, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJson(['message' => "Win"]);
    }

    /**
     * Validate Lose Response
     *
     * @return void
     */
    public function testLose()
    {
        $body = [
            'teamA' => '35,70,20,50,40',
            'teamB' => '35,10,30,20,90',
        ];

        $this->json('POST', '/check-result', $body, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJson(['message' => "Lose"]);
    }

    /**
     * Validate Input Param
     *
     * @return void
     */
    public function testTeamSize()
    {
        $body = [
            'teamA' => '100,20,50,40',
            'teamB' => '35,10,30,20,90',
        ];
        $this->json('POST', '/check-result', $body, ['Accept' => 'application/json'])
        ->assertStatus(400)
        ->assertJson(['message' => "Team Size is not same"]);
    }

    /**
     * Validate Input Param
     *
     * @return void
     */
    public function testInvalidTeamSize()
    {
        $body = [
            'teamA' => '100,20,50,40',
            'teamB' => '35,10,30,20,90',
        ];
        $this->json('POST', '/check-result', $body, ['Accept' => 'application/json'])
        ->assertStatus(400)
        ->assertJson(['message' => "Win"]);
    }
}
